# 5 Min Production App Quickstart

## Generate App

```
rails new rails6-app --database=postgresql
cd rails6-demo
git add .; git commit -m "Initial Commit"
```

## Generate Product scaffold

```
rails g scaffold Product title:string price:integer description:text picture:string
rake db:create
rake db:migrate

# add `root 'products#index'` to routes.rb

git add .; git commit -m "Adding Product scaffold"
```

## Configure GitLab 

1. Create a new project in GitLab, and setup your git remote
1. Configure CI Variables (Settings > CI/CD > Variables)
    * `AWS_ACCESS_KEY_ID`
    * `AWS_DEFAULT_REGION`
    * `AWS_SECRET_ACCESS_KEY`
    * `GL_VAR_RAILS_MASTER_KEY` (find this with `cat config/master.key`)
1. Add Dockerfile
1. Add .gitlab-ci.yml

```
git add .; git commit -m "Adding Dockerfile and CI"
git push origin master
```
